#20180502
	增加号码对象的自定义starter部分
	
	1、 PapSequenceAutoConfiguration 类文件，扫描对应的路径
	2、spring.factories 增加对应的启动类部分
	3、pom.xml 增加  spring-boot-autoconfigure 部分
	
#20190305
    新创建分支：
        考虑将 数据源的配置移除到外部。
        
    使用：(前提：数据源已经在外部被定义)
        
		<dependency>
			<groupId>com.pap</groupId>
			<artifactId>pap-sequence-starter</artifactId>
			<version>0.0.1-RELEASE</version>
		</dependency>
		
	
        @Autowired
        private ISequenceService sequenceService;
    
        @GetMapping(value = "/seq/{sequenceId}")
        public ResponseVO<String> selectInfoById(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                                  @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
                                                  @PathVariable("sequenceId") String sequenceId) throws Exception {
    
            return ResponseVO.successdata(sequenceService.selectNextSimpleSequence(sequenceId));
        }
        
#20190307
    新创建分支V2：
        解决考虑将 sequence 对应的功能完全抽离到一个独有的数据库中，由外部引入项目传递过来数据源进行处理。
        解决思路： 针对指定包名下的代码，指定对应的数据源。
        
    使用方法：
    1、 外部引入项目 pom.xml 和对应的操作类 增加如上方法；
    
    2、 外部引入项目，将数据源部分进行修改，增加 sequence 部分的数据源配置；
            spring.datasource.druid.sequence.type=com.alibaba.druid.pool.DruidDataSource
            spring.datasource.druid.sequence.url=jdbc:mysql://127.0.0.1:3306/seq?characterEncoding=utf8&useSSL=true&autoReconnect=true&serverTimezone=UTC
            spring.datasource.druid.sequence.driver-class-name=com.mysql.cj.jdbc.Driver
            spring.datasource.druid.sequence.username=root
            spring.datasource.druid.sequence.password=alexgaoyh
            
    3、 由于解决思路是使用类似 多数据源 的方式进行配置
        3.1、为了防止数据源重复创建，则启动类部分增加如下隔离
            @SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, MybatisAutoConfiguration.class})
            
        3.2、启动类部分隔离之后，则需要手动创建数据源相关的Bean。 同时如果默认有多个数据源的话，则需要配置到一个优先级，将手动创建的数据源，这些默认部分增加 @Primary 注解，优先找这一部分功能
                        
            @Value("${mybatis.mapperLocations}")
            private String mapperLocations;
            
            @Primary
            @Bean(name = "defaultDataSource")
            @ConfigurationProperties("spring.datasource.druid.default")
            public DataSource defaultDataSource(){
                return DruidDataSourceBuilder.create().build();
            }
    
            @Primary
            @Bean(name = "defaultTransactionManager")
            public DataSourceTransactionManager defaultTransactionManager() throws SQLException {
                return new DataSourceTransactionManager(defaultDataSource());
            }
    
            @Primary
            @Bean(name = "defaultSqlSessionFactory")
            public SqlSessionFactory defaultSqlSessionFactory(@Qualifier("defaultDataSource") DataSource defaultDataSource) throws Exception {
                final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
                sessionFactory.setDataSource(defaultDataSource);
                sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                        .getResources(mapperLocations));
    
                return sessionFactory.getObject();
            }
        
            