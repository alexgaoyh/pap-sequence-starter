/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50724
Source Host           : 127.0.0.1:3306
Source Database       : cf

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-03-05 16:42:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_sys_sequence
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_sequence`;
CREATE TABLE `t_sys_sequence` (
  `sequence_id` varchar(32) NOT NULL COMMENT '编号',
  `sequence_code` varchar(32) DEFAULT NULL COMMENT '编码',
  `sequence_remark` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `project_code` varchar(32) DEFAULT NULL COMMENT '所属项目编号',
  `project_name` varchar(32) DEFAULT NULL COMMENT '所属项目名称',
  `module_code` varchar(32) DEFAULT NULL COMMENT '所属模块编码',
  `module_name` varchar(32) DEFAULT NULL COMMENT '所属模块名称',
  `sequence_no_length` int(11) DEFAULT NULL COMMENT '序号长度',
  `sequence_prefix` varchar(32) DEFAULT NULL COMMENT '序号前缀',
  `sequence_suffix` varchar(32) DEFAULT NULL COMMENT '序号后缀',
  `sequence_curr_no` int(11) DEFAULT NULL COMMENT '序号当前值',
  `sequence_step` int(11) DEFAULT NULL COMMENT '序号步长',
  `now_year` int(11) DEFAULT NULL COMMENT '当前年',
  `now_month` int(11) DEFAULT NULL COMMENT '当前月',
  `now_date` int(11) DEFAULT NULL COMMENT '当前日',
  `version` int(32) DEFAULT NULL COMMENT '版本号(乐观锁)',
  PRIMARY KEY (`sequence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
