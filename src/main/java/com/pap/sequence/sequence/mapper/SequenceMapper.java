package com.pap.sequence.sequence.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.sequence.sequence.entity.Sequence;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface SequenceMapper extends PapBaseMapper<Sequence> {

    Sequence selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    Sequence selectByPrimaryKey(String sequenceId);

    // alexgaoyh add
    int updateByPrimaryKeyAndVersion(Sequence record);
}