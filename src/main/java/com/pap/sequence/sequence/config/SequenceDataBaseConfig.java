package com.pap.sequence.sequence.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/3/7 14:11
 * @Description:
 */
@Component
@MapperScan(basePackages = SequenceDataBaseConfig.PACKAGE, sqlSessionFactoryRef = "sequenceSqlSessionFactory")
@ConfigurationProperties(prefix = "spring.datasource.druid.sequence")
public class SequenceDataBaseConfig {


    /**
     * dao层的包路径
     */
    static final String PACKAGE = "com.pap.sequence.sequence.mapper";

    /**
     * mapper文件的相对路径
     */
    private static final String MAPPER_LOCATION = "classpath:com/pap/sequence/sequence/mapper/*.xml";

    @Value("${spring.datasource.druid.sequence.url}")
    private String url;

    @Value("${spring.datasource.druid.sequence.username}")
    private String username;

    @Value("${spring.datasource.druid.sequence.password}")
    private String password;

    @Value("${spring.datasource.druid.sequence.driver-class-name}")
    private String driverClassName;

    private int initialSize = 1;
    private int minIdle = 1;
    private int maxActive = 1;
    private long maxWait = 1;
    private long timeBetweenEvictionRunsMillis = 60000L;
    private long minEvictableIdleTimeMillis = 300000L;
    private String validationQuery = "SELECT 1 FROM DUAL";
    private boolean testWhileIdle = true;
    private boolean testOnBorrow = false;
    private boolean testOnReturn = false;
    private boolean poolPreparedStatements = true;
    private int maxPoolPreparedStatementPerConnectionSize = 20;

    @Bean(name = "sequenceDataSource")
    public DataSource sequenceDataSource() throws SQLException {
        DruidDataSource druid = new DruidDataSource();

        // 配置基本属性
        druid.setDriverClassName(driverClassName);
        druid.setUsername(username);
        druid.setPassword(password);
        druid.setUrl(url);

        //初始化时建立物理连接的个数
        druid.setInitialSize(initialSize);
        //最大连接池数量
        druid.setMaxActive(maxActive);
        //最小连接池数量
        druid.setMinIdle(minIdle);
        //获取连接时最大等待时间，单位毫秒。
        druid.setMaxWait(maxWait);
        //间隔多久进行一次检测，检测需要关闭的空闲连接
        druid.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        //一个连接在池中最小生存的时间
        druid.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        //用来检测连接是否有效的sql
        druid.setValidationQuery(validationQuery);
        //建议配置为true，不影响性能，并且保证安全性。
        druid.setTestWhileIdle(testWhileIdle);
        //申请连接时执行validationQuery检测连接是否有效
        druid.setTestOnBorrow(testOnBorrow);
        druid.setTestOnReturn(testOnReturn);
        //是否缓存preparedStatement，也就是PSCache，oracle设为true，mysql设为false。分库分表较多推荐设置为false
        druid.setPoolPreparedStatements(poolPreparedStatements);
        // 打开PSCache时，指定每个连接上PSCache的大小
        druid.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);

        return druid;
    }

    @Bean(name = "sequenceTransactionManager")
    public DataSourceTransactionManager sequenceTransactionManager() throws SQLException {
        return new DataSourceTransactionManager(sequenceDataSource());
    }

    @Bean(name = "sequenceSqlSessionFactory")
    public SqlSessionFactory sequenceSqlSessionFactory(@Qualifier("sequenceDataSource") DataSource sequenceDataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(sequenceDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(SequenceDataBaseConfig.MAPPER_LOCATION));

        return sessionFactory.getObject();
    }



}
