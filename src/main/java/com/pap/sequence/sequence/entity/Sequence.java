package com.pap.sequence.sequence.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_sys_sequence", namespace = "com.pap.sequence.sequence.mapper.SequenceMapper", remarks = " 修改点 ", aliasName = "t_sys_sequence t_sys_sequence" )
public class Sequence extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_sys_sequence.sequence_id
     */
    @MyBatisColumnAnnotation(name = "sequence_id", value = "t_sys_sequence_sequence_id", chineseNote = "编号", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String sequenceId;

    /**
     *  编码,所属表字段为t_sys_sequence.sequence_code
     */
    @MyBatisColumnAnnotation(name = "sequence_code", value = "t_sys_sequence_sequence_code", chineseNote = "编码", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String sequenceCode;

    /**
     *  备注,所属表字段为t_sys_sequence.sequence_remark
     */
    @MyBatisColumnAnnotation(name = "sequence_remark", value = "t_sys_sequence_sequence_remark", chineseNote = "备注", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String sequenceRemark;

    /**
     *  所属项目编号,所属表字段为t_sys_sequence.project_code
     */
    @MyBatisColumnAnnotation(name = "project_code", value = "t_sys_sequence_project_code", chineseNote = "所属项目编号", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "所属项目编号")
    private String projectCode;

    /**
     *  所属项目名称,所属表字段为t_sys_sequence.project_name
     */
    @MyBatisColumnAnnotation(name = "project_name", value = "t_sys_sequence_project_name", chineseNote = "所属项目名称", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "所属项目名称")
    private String projectName;

    /**
     *  所属模块编码,所属表字段为t_sys_sequence.module_code
     */
    @MyBatisColumnAnnotation(name = "module_code", value = "t_sys_sequence_module_code", chineseNote = "所属模块编码", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "所属模块编码")
    private String moduleCode;

    /**
     *  所属模块名称,所属表字段为t_sys_sequence.module_name
     */
    @MyBatisColumnAnnotation(name = "module_name", value = "t_sys_sequence_module_name", chineseNote = "所属模块名称", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "所属模块名称")
    private String moduleName;

    /**
     *  序号长度,所属表字段为t_sys_sequence.sequence_no_length
     */
    @MyBatisColumnAnnotation(name = "sequence_no_length", value = "t_sys_sequence_sequence_no_length", chineseNote = "序号长度", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "序号长度")
    private Integer sequenceNoLength;

    /**
     *  序号前缀,所属表字段为t_sys_sequence.sequence_prefix
     */
    @MyBatisColumnAnnotation(name = "sequence_prefix", value = "t_sys_sequence_sequence_prefix", chineseNote = "序号前缀", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "序号前缀")
    private String sequencePrefix;

    /**
     *  序号后缀,所属表字段为t_sys_sequence.sequence_suffix
     */
    @MyBatisColumnAnnotation(name = "sequence_suffix", value = "t_sys_sequence_sequence_suffix", chineseNote = "序号后缀", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "序号后缀")
    private String sequenceSuffix;

    /**
     *  序号当前值,所属表字段为t_sys_sequence.sequence_curr_no
     */
    @MyBatisColumnAnnotation(name = "sequence_curr_no", value = "t_sys_sequence_sequence_curr_no", chineseNote = "序号当前值", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "序号当前值")
    private Integer sequenceCurrNo;

    /**
     *  序号步长,所属表字段为t_sys_sequence.sequence_step
     */
    @MyBatisColumnAnnotation(name = "sequence_step", value = "t_sys_sequence_sequence_step", chineseNote = "序号步长", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "序号步长")
    private Integer sequenceStep;

    /**
     *  当前年,所属表字段为t_sys_sequence.now_year
     */
    @MyBatisColumnAnnotation(name = "now_year", value = "t_sys_sequence_now_year", chineseNote = "当前年", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "当前年")
    private Integer nowYear;

    /**
     *  当前月,所属表字段为t_sys_sequence.now_month
     */
    @MyBatisColumnAnnotation(name = "now_month", value = "t_sys_sequence_now_month", chineseNote = "当前月", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "当前月")
    private Integer nowMonth;

    /**
     *  当前日,所属表字段为t_sys_sequence.now_date
     */
    @MyBatisColumnAnnotation(name = "now_date", value = "t_sys_sequence_now_date", chineseNote = "当前日", tableAlias = "t_sys_sequence")
    @MyApiModelPropertyAnnotation(value = "当前日")
    private Integer nowDate;

    /**
     *  版本号(乐观锁),所属表字段为t_sys_sequence.version
     */
    private Integer version;

    private static final long serialVersionUID = 1L;

    public String getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(String sequenceId) {
        this.sequenceId = sequenceId;
    }

    public String getSequenceCode() {
        return sequenceCode;
    }

    public void setSequenceCode(String sequenceCode) {
        this.sequenceCode = sequenceCode;
    }

    public String getSequenceRemark() {
        return sequenceRemark;
    }

    public void setSequenceRemark(String sequenceRemark) {
        this.sequenceRemark = sequenceRemark;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Integer getSequenceNoLength() {
        return sequenceNoLength;
    }

    public void setSequenceNoLength(Integer sequenceNoLength) {
        this.sequenceNoLength = sequenceNoLength;
    }

    public String getSequencePrefix() {
        return sequencePrefix;
    }

    public void setSequencePrefix(String sequencePrefix) {
        this.sequencePrefix = sequencePrefix;
    }

    public String getSequenceSuffix() {
        return sequenceSuffix;
    }

    public void setSequenceSuffix(String sequenceSuffix) {
        this.sequenceSuffix = sequenceSuffix;
    }

    public Integer getSequenceCurrNo() {
        return sequenceCurrNo;
    }

    public void setSequenceCurrNo(Integer sequenceCurrNo) {
        this.sequenceCurrNo = sequenceCurrNo;
    }

    public Integer getSequenceStep() {
        return sequenceStep;
    }

    public void setSequenceStep(Integer sequenceStep) {
        this.sequenceStep = sequenceStep;
    }

    public Integer getNowYear() {
        return nowYear;
    }

    public void setNowYear(Integer nowYear) {
        this.nowYear = nowYear;
    }

    public Integer getNowMonth() {
        return nowMonth;
    }

    public void setNowMonth(Integer nowMonth) {
        this.nowMonth = nowMonth;
    }

    public Integer getNowDate() {
        return nowDate;
    }

    public void setNowDate(Integer nowDate) {
        this.nowDate = nowDate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sequenceId=").append(sequenceId);
        sb.append(", sequenceCode=").append(sequenceCode);
        sb.append(", sequenceRemark=").append(sequenceRemark);
        sb.append(", projectCode=").append(projectCode);
        sb.append(", projectName=").append(projectName);
        sb.append(", moduleCode=").append(moduleCode);
        sb.append(", moduleName=").append(moduleName);
        sb.append(", sequenceNoLength=").append(sequenceNoLength);
        sb.append(", sequencePrefix=").append(sequencePrefix);
        sb.append(", sequenceSuffix=").append(sequenceSuffix);
        sb.append(", sequenceCurrNo=").append(sequenceCurrNo);
        sb.append(", sequenceStep=").append(sequenceStep);
        sb.append(", nowYear=").append(nowYear);
        sb.append(", nowMonth=").append(nowMonth);
        sb.append(", nowDate=").append(nowDate);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }
}