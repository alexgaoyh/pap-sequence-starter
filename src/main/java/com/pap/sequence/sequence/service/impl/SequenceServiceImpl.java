package com.pap.sequence.sequence.service.impl;

import com.pap.base.util.date.DateUtils;
import com.pap.base.util.string.StringUtilss;
import com.pap.sequence.sequence.mapper.SequenceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.sequence.sequence.entity.Sequence;
import com.pap.sequence.sequence.service.ISequenceService;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

public class SequenceServiceImpl implements ISequenceService {

    @Autowired
    private SequenceMapper sequenceMapper;

    @Override
    public String selectNextSimpleSequence(String id) {
        String returnSeqStr = "";
        Sequence sysSequence = sequenceMapper.selectByPrimaryKey(id);
        if(sysSequence != null) {
            String prefixStr = sysSequence.getSequencePrefix();
            String suffixStr = sysSequence.getSequenceSuffix();
            int currNoStr = sysSequence.getSequenceCurrNo();
            int stepStr = sysSequence.getSequenceStep();
            int seqNoLengthStr = sysSequence.getSequenceNoLength();


            BigDecimal nextNoDecimal = new BigDecimal(currNoStr).add(new BigDecimal(stepStr));

            // 前缀
            if(StringUtilss.isNotEmpty(prefixStr)) {
                returnSeqStr = returnSeqStr + prefixStr;
            }
            // 操作值
            returnSeqStr = returnSeqStr + StringUtilss.addZeroForNum(nextNoDecimal + "", seqNoLengthStr);

            // 后缀
            if(StringUtilss.isNotEmpty(suffixStr)) {
                returnSeqStr = returnSeqStr + suffixStr;
            }

            Sequence temp = new Sequence();
            temp.setSequenceId(sysSequence.getSequenceId());
            temp.setSequenceCurrNo(nextNoDecimal.intValue());
            temp.setVersion(sysSequence.getVersion());
            int operInt = sequenceMapper.updateByPrimaryKeyAndVersion(temp);
            if(operInt == 1) {
                return returnSeqStr;
            }

        }
        return null;
    }

    @Override
    public String selectNextSimpleSequenceWithDate(String id) {
        String returnSeqStr = "";
        Sequence sysSequence = sequenceMapper.selectByPrimaryKey(id);
        if(sysSequence != null) {
            String prefixStr = sysSequence.getSequencePrefix();
            String suffixStr = sysSequence.getSequenceSuffix();
            int currNoStr = sysSequence.getSequenceCurrNo();
            int stepStr = sysSequence.getSequenceStep();
            int seqNoLengthStr = sysSequence.getSequenceNoLength();


            BigDecimal nextNoDecimal = new BigDecimal(currNoStr).add(new BigDecimal(stepStr));

            // 前缀
            if(StringUtilss.isNotEmpty(prefixStr)) {
                returnSeqStr = returnSeqStr + prefixStr;
            }
            // 年月日
            returnSeqStr = returnSeqStr + DateUtils.format(new Date(), DateUtils.YYYYMMDD);
            // 操作值
            returnSeqStr = returnSeqStr + StringUtilss.addZeroForNum(nextNoDecimal + "", seqNoLengthStr);

            // 后缀
            if(StringUtilss.isNotEmpty(suffixStr)) {
                returnSeqStr = returnSeqStr + suffixStr;
            }

            Sequence temp = new Sequence();
            temp.setSequenceId(sysSequence.getSequenceId());
            temp.setSequenceCurrNo(nextNoDecimal.intValue());
            temp.setVersion(sysSequence.getVersion());
            int operInt = sequenceMapper.updateByPrimaryKeyAndVersion(temp);
            if(operInt == 1) {
                return returnSeqStr;
            }

        }
        return null;
    }

    @Override
    public String selectNextSimpleSequenceOutPersist(String id) {
        String returnSeqStr = "";
        Sequence sysSequence = sequenceMapper.selectByPrimaryKey(id);
        if(sysSequence != null) {
            String prefixStr = sysSequence.getSequencePrefix();
            String suffixStr = sysSequence.getSequenceSuffix();
            int currNoStr = sysSequence.getSequenceCurrNo();
            int stepStr = sysSequence.getSequenceStep();
            int seqNoLengthStr = sysSequence.getSequenceNoLength();


            BigDecimal nextNoDecimal = new BigDecimal(currNoStr).add(new BigDecimal(stepStr));

            // 前缀
            if(StringUtilss.isNotEmpty(prefixStr)) {
                returnSeqStr = returnSeqStr + prefixStr;
            }
            // 操作值
            returnSeqStr = returnSeqStr + StringUtilss.addZeroForNum(nextNoDecimal + "", seqNoLengthStr);

            // 后缀
            if(StringUtilss.isNotEmpty(suffixStr)) {
                returnSeqStr = returnSeqStr + suffixStr;
            }

            return returnSeqStr;

        }
        return null;
    }

    @Override
    public String selectNextSimpleSequenceWithDateOutPersist(String id) {
        String returnSeqStr = "";
        Sequence sysSequence = sequenceMapper.selectByPrimaryKey(id);
        if(sysSequence != null) {
            String prefixStr = sysSequence.getSequencePrefix();
            String suffixStr = sysSequence.getSequenceSuffix();
            int currNoStr = sysSequence.getSequenceCurrNo();
            int stepStr = sysSequence.getSequenceStep();
            int seqNoLengthStr = sysSequence.getSequenceNoLength();


            BigDecimal nextNoDecimal = new BigDecimal(currNoStr).add(new BigDecimal(stepStr));

            // 前缀
            if(StringUtilss.isNotEmpty(prefixStr)) {
                returnSeqStr = returnSeqStr + prefixStr;
            }
            // 年月日
            returnSeqStr = returnSeqStr + DateUtils.format(new Date(), DateUtils.YYYYMMDD);
            // 操作值
            returnSeqStr = returnSeqStr + StringUtilss.addZeroForNum(nextNoDecimal + "", seqNoLengthStr);

            // 后缀
            if(StringUtilss.isNotEmpty(suffixStr)) {
                returnSeqStr = returnSeqStr + suffixStr;
            }

            return returnSeqStr;

        }
        return null;
    }
}