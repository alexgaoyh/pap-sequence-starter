package com.pap.sequence.sequence.service;

public interface ISequenceService {

    /**
     * 查询下一个序列号，不使用年月日，只使用前后缀,当前值,序列长度
     * @param id
     * @return
     */
    String selectNextSimpleSequence(String id);

    /**
     * 查询下一个序列号，包含使用年月日，使用前后缀,年月日,当前值,序列长度
     * @param id
     * @return
     */
    String selectNextSimpleSequenceWithDate(String id);

    /**
     * 查询下一个序列号，不使用年月日，只使用前后缀,当前值,序列长度
     *  并不进行持久化数据库，只用来查询展示
     * @param id
     * @return
     */
    String selectNextSimpleSequenceOutPersist(String id);

    /**
     * 查询下一个序列号，使用年月日，只使用前后缀,当前值,序列长度
     *  并不进行持久化数据库，只用来查询展示
     * @param id
     * @return
     */
    String selectNextSimpleSequenceWithDateOutPersist(String id);
}
