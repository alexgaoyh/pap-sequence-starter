package com.pap.sequence;

import com.pap.sequence.sequence.config.SequenceDataBaseConfig;
import com.pap.sequence.sequence.service.ISequenceService;
import com.pap.sequence.sequence.service.impl.SequenceServiceImpl;
import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration//开启配置
@ConditionalOnClass({ISequenceService.class})//存在 ISysSequenceAgent 时初始化该配置类
@EnableConfigurationProperties({SequenceDataBaseConfig.class})
public class PapSequenceAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean(DatabaseIdProvider.class)
	public DatabaseIdProvider getDatabaseIdProvider() {
		DatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
		Properties p = new Properties();
		p.setProperty("Oracle", "oracle");
		p.setProperty("MySQL", "mysql");
		databaseIdProvider.setProperties(p);
		return databaseIdProvider;
	}

	@Bean//创建 ISequenceService 实体bean
	@ConditionalOnMissingBean(ISequenceService.class)//缺失ISysSequenceAgent实体bean时，初始化ISysSequenceAgent并添加到SpringIoc
	public ISequenceService sequenceService() {
        ISequenceService sequenceService = new SequenceServiceImpl();
        return sequenceService;
	}

}
